const tabsContainerWorkBlock = document.getElementById("titles-work-block")
  .children;
const imagesContainer = document.getElementById("images-container-work-block");
const loadMoreButton = document.getElementById("loadMoreButton");
for (var i = 0, child; (child = tabsContainerWorkBlock[i]); i++) {
  child.setAttribute("id", `tabWorkBlock-${i}`);
  const tabName = child.textContent.toLowerCase().replace(/ /, "-");
  child.setAttribute("onclick", `showImages("${tabName}", ${i})`);
}
loadMoreButton.setAttribute("onclick", `loadMore()`);
let activeTabName;

showImages("all", 0);

function showImages(tabName, tabIndex) {
  loadMoreButton.hidden = false;
  activeTabName = tabName;
  imagesContainer.innerHTML = "";
  for (let i = 0; i < tabsContainerWorkBlock.length; i++) {
    if (+tabsContainerWorkBlock[i].id.substring(13) === tabIndex) {
      document.getElementById(`tabWorkBlock-${i}`).classList.add("active");
    } else {
      document.getElementById(`tabWorkBlock-${i}`).classList.remove("active");
    }
  }

  const tabsArray = [
    "graphic-design",
    "web-design",
    "app-design",
    "landing-page",
    "wordpress",
  ];
  let filteredBlocks = "";
  for (let i = 1; i < 13; i++) {
    if (activeTabName == "all") {
      const random = Math.floor(Math.random() * tabsArray.length);
      tabName = tabsArray[random];
    }

    filteredBlocks += `
        <div class="image-item-work-block">
        <img class="img-work-block" src="./images/${tabName}/${
      tabName + i
    }.jpg"/>
        <div class="image-reverse-side-work-block">
          <div class="round-btns-container">
            <button class="round-btn">
              <img src="./images/roundButton1.png" />
            </button>
            <button class="round-btn filled-btn">
              <img src="./images/roundButton2.png" />
            </button>
          </div>
          <h1 class="h1-reverse-side-work-block">creative design</h1>
          <p class="p-reverse-side-work-block">Web Design</p>
        </div>
      </div>`;
  }
  imagesContainer.insertAdjacentHTML("beforeend", filteredBlocks);
}

function loadMore() {
  let tabName = activeTabName;
  const tabsArray = [
    "graphic-design",
    "web-design",
    "app-design",
    "landing-page",
    "wordpress",
  ];
  let secondPortionOfImages =
    '<div id="images-container-work-block-2" class="images-container-work-block">';
  for (let i = 1; i < 13; i++) {
    if (activeTabName == "all") {
      const random = Math.floor(Math.random() * tabsArray.length);
      tabName = tabsArray[random];
    }
    secondPortionOfImages += `
        <div class="image-item-work-block">
        <img class="img-work-block" src="./images/${tabName}/${
      tabName + i
    }.jpg"/>
        <div class="image-reverse-side-work-block">
          <div class="round-btns-container"> 
            <button class="round-btn">
              <img src="./images/roundButton1.png" />
            </button>
            <button class="round-btn filled-btn">
              <img src="./images/roundButton2.png" />
            </button>
          </div>
          <h1 class="h1-reverse-side-work-block">creative design</h1>
          <p class="p-reverse-side-work-block">Web Design</p>
        </div>
      </div>`;
  }
  secondPortionOfImages += "</div>";
  imagesContainer.insertAdjacentHTML("beforeend", secondPortionOfImages);
  loadMoreButton.hidden = true;
}
