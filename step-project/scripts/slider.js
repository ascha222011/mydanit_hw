$(document).ready(function () {
  $(".slider").slick({
    arrows: true,
    dots: false,
    slidesToShow: 1,
    autoplay: false,
    speed: 1000,
    autoplaySpeed: 800,
    asNavFor: ".sliderDots",
  });
});

$(document).ready(function () {
  $(".sliderDots").slick({
    arrows: false,
    slidesToShow: 3,
    asNavFor: ".slider",
    focusOnSelect: true,
  });
});

$(document).ready(function () {
  $(".slider3").slick({
    arrows: true,
    dots: false,
    slidesToShow: 1,
    autoplay: false,
    speed: 1000,
    autoplaySpeed: 800,
    asNavFor: ".sliderDots",
  });
});
