import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import React, { Component } from "react";
import "./App.scss";

class App extends Component {
  constructor() {
    super();
    this.modalFirst = React.createRef();
    this.modalSecond = React.createRef();
  }

  openFirstModal = (e) => {
    this.modalFirst.current.handleClick(e);
  };

  openSecondModal = (e) => {
    this.modalSecond.current.handleClick(e);
  };

  render() {
    return (
      <div>
        <Modal
          ref={this.modalFirst}
          header="Header first modal"
          text="Text first modal"
          closebuton
          handleClose={this.closeMenu}
          actions={
            <div>
              <Button
                color="white"
                backgroundColor="#CD5C5C"
                text="Ok" onClick={this.closeMenu}
              />
               <Button
                color="white"
                backgroundColor="#CD5C5C"
                text="Cancel" onClick={this.closeMenu}
              />
            </div>
          }
        >
          <p>Modal</p>
        </Modal>
        <Modal
          ref={this.modalSecond}
          header="Header second modal"
          text="Text second modal"
          closebuton={false}
          handleClose={this.closeMenu}
          actions={
            <div>
              <Button
                color="white"
                backgroundColor="purple"
                text="Action" onClick={this.closeMenu}
              />
            </div>
          }
        >
          <p>Modal</p>
        </Modal>

        <Button
          color="white"
          backgroundColor="blue"
          text="Open first modal"
          onClick={this.openFirstModal}
        />
        <Button
          color="white"
          backgroundColor="grey"
          text="Open second modal"
          onClick={this.openSecondModal}
        />
      </div>
    );
  }
}

export default App;
