import React, { PureComponent } from "react";
import "./Button.scss";
//import PropTypes from 'prop-types'

class Button extends PureComponent {
  render() {
    const { backgroundColor, text, onClick } = this.props;

    const styles = {
      backgroundColor,
      color: "white"
    };

    return (
        <button style={styles} onClick={onClick}>
          {text}
        </button>
    );
  }
}

/*Button.propTypes = {
    title: PropTypes.string,
    backgroundColor: PropTypes.string,
    color: PropTypes.string,
    onClick: propTypes.func.isRequired
};*/

export default Button;
