let prevNum;

function keyPress(e) {
  let keyNum, idCurrent, idPrevious;
  
  if (prevNum == 13) idPrevious = "Enter";
  else idPrevious = String.fromCharCode(prevNum);
  const buttonPrev = document.getElementById(idPrevious);

  if (window.event) {
    keyNum = window.event.keyCode;
  } else if (e) {
    keyNum = e.which;
  }

  prevNum = keyNum;

  if (keyNum == 13) idCurrent = "Enter";
  else idCurrent = String.fromCharCode(keyNum);
  const buttonNext = document.getElementById(idCurrent);
  
  if (buttonNext)
    buttonNext.setAttribute("style", "border: 2px solid #4CAF50;");
  if (buttonPrev) buttonPrev.setAttribute("style", "");
}
document.onkeydown = keyPress;
