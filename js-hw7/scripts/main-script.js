var numbers = ["hello", "world", "Kiev", [1, 5, 6], "Odessa", "Lviv"];
showArray(numbers);

function showArray(arr) {
  let ol = document.createElement("ol");
  document.body.appendChild(ol);

  arr.map(function (elem) {
    let liLast = document.createElement("li");
    liLast.innerHTML = elem;
    ol.append(liLast);
  });
}
