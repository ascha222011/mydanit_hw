class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(employeeName) {
    this._name = employeeName;
  }
  get age() {
    return this._age;
  }
  set age(employeeAge) {
    this._age = employeeAge;
  }

  get salary() {
    return this._salary;
  }
  set salary(employeeSalary) {
    this._salary = employeeSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
  get lang() {
    return this._lang;
  }
  set lang(employeeLang) {
    this._lang = employeeLang;
  }

  toString() {
    console.log(
      "Employee name " +
        this.name +
        "with " +
        this.salary +
        " salary, and them age is - " +
        this.age +
        ". Array of known languages is " +
        this.lang
    );
  }
}

const juniorFullStack = new Programmer("Tom", 24, 500, ["EU", "UK", "RU"]);
juniorFullStack.toString();
const middleFe = new Programmer("Nika", 27, 800, ["EU", "UK", "RU"]);
middleFe.toString();
const seniorBE = new Programmer("Eugene", 35, 1100, ["FR,DE"]);
seniorBE.toString();
