import React, { useState } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import "./Product.scss";
import Icon from "../Icon/Icon";

function Product(props) {
  const { id, name, price, image, code, color } = props.product;
//do 1 get from 2
  const getProductInCart = () => {
    const addedProductsToCartJSON = localStorage.getItem("addedProductsToCart");
    return JSON.parse(addedProductsToCartJSON) || [];
  };

  const checkIsAddedToCart = () => {
    const addedProductsToCart = getProductInCart();
    return addedProductsToCart.includes(id);
  };

  const getFavoriteProducts = () => {
    const markedAsFavoriteProductsJSON = localStorage.getItem(
      "markedAsFavoriteProducts"
    );
    return JSON.parse(markedAsFavoriteProductsJSON) || [];
  };

  const checkIsFavorite = () => {
    const markedProductAsFavorite = getFavoriteProducts();
    return markedProductAsFavorite.includes(id);
  };

  const [isAddedToCart, setIsAddedToCart] = useState(checkIsAddedToCart());
  const [isFavorite, setIsFavorite] = useState(checkIsFavorite());

  const modalFirst = React.createRef();

  const openCartConfirmationModal = (e) => {
    modalFirst.current.handleClick(e);
  };

  const closeMenu = (e) => {
    modalFirst.current.closeMenu(e);
  };

  const addToCart = (e) => {
    let addedProductsToCart = [];
    if (localStorage.getItem("addedProductsToCart")) {
      addedProductsToCart = getProductInCart();
    }
    addedProductsToCart.push(id);
    setIsAddedToCart(true);

    localStorage.setItem(
      "addedProductsToCart",
      JSON.stringify(addedProductsToCart)
    );
    closeMenu(e);
  };

  //rename
  const fillIcon = () => {
   
    let markedAsFavoriteProducts = [];
    if (localStorage.getItem("markedAsFavoriteProducts")) {
      markedAsFavoriteProducts = getFavoriteProducts();
    }
    if (!checkIsFavorite()) {
      markedAsFavoriteProducts.push(id);
      setIsFavorite(true);

    } else {
      const indexInFavoritsList = markedAsFavoriteProducts.indexOf(id);
      markedAsFavoriteProducts.splice(indexInFavoritsList, 1);
      setIsFavorite(false);
    }
    localStorage.setItem(
      "markedAsFavoriteProducts",
      JSON.stringify(markedAsFavoriteProducts)
    );
  };

  return (
    <div className="product-container">
      <img className="product-image" alt={name} src={image} />
      <h3> {name}</h3>
      <p>
        {code} / {color}
      </p>
      <h2> {price} $</h2>
      <div className="actions-container">
        <Button
          color="white"
          backgroundColor="#0ec4ba"
          text="Add to cart"
          disabled={isAddedToCart}
          onClick={openCartConfirmationModal}
        />
        <Icon filled={isFavorite? true : false} type="star" color="#FFD700"  onClick={fillIcon} />
      </div>

      <Modal
        ref={modalFirst}
        header="Are you sure you want to add this item to cart?"
        text="You can remove item from cart in any time"
        handleClose={closeMenu}
        actions={
          <>
            <Button
              color="white"
              backgroundColor="#007dff"
              text="YES!"
              onClick={addToCart}
            />
            <Button
              color="white"
              backgroundColor="#c9e5ee"
              text="Cancel"
              onClick={closeMenu}
            />
          </>
        }
      >
        <p>Modal</p>
      </Modal>
    </div>
  );
}

Product.defaultProps = {};

Product.propTypes = {};

export default Product;
