import React from 'react';
import * as icons from '../../theme/icons';

function Icon(props) {
  const { type, color, filled, onClick } = props;

  const iconJsx = icons[type];

  const styles = {
    margin: "20px"
  }

  if (!iconJsx) {
    return null;
  }

  return (
    <span style={styles} onClick={onClick}>{iconJsx(color, filled)}</span>
  )
}

export default Icon
