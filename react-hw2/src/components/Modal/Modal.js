import React, { PureComponent } from "react";
import "./Modal.scss";
//import PropTypes from 'prop-types'
import Button from "../Button/Button.js";

class Modal extends PureComponent {
  constructor() {
    super();
    this.state = {
      show: false,
    };
  }

  handleClick = (event) => {
    event.preventDefault();
   
    this.setState({ show: true }, () => {
      document.addEventListener("mouseup", this.closeMenu);
      document.getElementById("modal-actions-container").addEventListener("mouseup", (e) => e.stopPropagation());
    });
  };


  closeMenu = () => {
    console.log("!!");
    this.setState({ show: false }, () => {
      document.removeEventListener("mouseup", this.closeMenu);
    });
  };

  render() {
    const { header, text, closebuton, handleClose, actions } = this.props;

    return (
      <>
        {this.state.show && (
          <div className="modal display-block">
            <section className="modal-main">
              <div className="header-container">
                <p className="header-title">{header}</p>
                {closebuton && (
                  <button className="x-button" onClick={handleClose}>
                    X
                  </button>
                )}
              </div>
              <p className="main-text"> {text}</p>
              <div id="modal-actions-container" className="modal-actions-container">{actions}</div>
            </section>
          </div>
        )}
      </>
    );
  }
}

/*Button.propTypes = {
    title: PropTypes.string,
    backgroundColor: PropTypes.string,
    color: PropTypes.string,
    onClick: propTypes.func.isRequired
};*/

export default Modal;
