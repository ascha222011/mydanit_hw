import React from "react";
import Product from "../Product/Product";
import PropTypes from "prop-types";
import "./ProductCards.scss";

const ProductCards = (props) => {
  const { products } = props;

  const productCards = products
    .filter((product) => !product.hidden)
    .map((product) => <Product key={product.id} product={product} />);

  return <div className="products-container">{productCards}</div>;
};

ProductCards.propTypes = {
  products: PropTypes.array,
};

ProductCards.defaultProps = {
  products: [],
};

export default ProductCards;
