import React, { PureComponent } from "react";
import "./Button.scss";
//import PropTypes from 'prop-types'

class Button extends PureComponent {
  constructor() {
    super();
    this.state = {
      disabledButton: false,
    };
  }

  render() {
    const { backgroundColor, text, onClick, disabled } = this.props;

    const styles = {
      backgroundColor: disabled ? "gray" : backgroundColor,
    };

    return (
      <button style={styles} disabled={disabled} onClick={onClick}>
        {text}
      </button>
    );
  }
}

/*Button.propTypes = {
    title: PropTypes.string,
    backgroundColor: PropTypes.string,
    color: PropTypes.string,
    onClick: propTypes.func.isRequired
};*/

export default Button;
