import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import React, { Component } from "react";
import "./App.scss";
import axios from "axios";
import ProductCards from "./components/ProductCards/ProductCards";

class App extends Component {
  constructor() {
    super();
    this.state = {
      products: [],
    };
  }

  getProducts = () => {
    axios.get("/products.json").then((res) => {
      this.setState({ products: res.data });
    });
  };

  componentDidMount() {
    this.getProducts();
  }

  render() {
    return (
      <div>
        <ProductCards products={this.state.products} />
      </div>
    );
  }
}

export default App;
