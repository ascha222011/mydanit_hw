
function showContent(id) {
    const input = document.getElementById(id);
    const iForInput= input.nextElementSibling;
    if(input.hasAttribute("type")){
        input.removeAttribute("type");
        iForInput.setAttribute("class", "fas fa-eye-slash icon-password");
    }
    else {
        input.setAttribute("type", "password");
        iForInput.setAttribute("class", "fas fa-eye icon-password");
   }
}

function checkPassword () {
    const errorElement = document.getElementById("error-message");
    const popupDiv = document.getElementById("popup");
    if(document.getElementById("firstPasswordInput").value == document.getElementById("secondPasswordInput").value){
        errorElement.innerHTML="";
        popupDiv.removeAttribute("hidden");
    }
    else {
        errorElement.innerHTML = "Нужно ввести одинаковые значения";
    }
}

function closePopup() {
    const popupDiv = document.getElementById("popup");
    popupDiv.setAttribute("hidden", true);
}