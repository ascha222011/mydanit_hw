const { src, dest } = require("gulp");
const imageMin = require('gulp-imagemin'); 
const browserSync = require("browser-sync").create();

const images = () => {
    return src('./src/img/*')
        .pipe(imageMin())         
        .pipe(dest("./dist/img/"))
        .pipe(browserSync.reload({
            stream: true
        }))
};

exports.images = images;
exports.browserSync = browserSync;
