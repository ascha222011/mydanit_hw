const { parallel, series } = require("gulp");

const serveTask = require("./gulp-tasks/serve").serve;
const watchTask = require("./gulp-tasks/watch").watch;
const scriptsTask = require("./gulp-tasks/scripts").scripts;
const stylesTask = require("./gulp-tasks/styles").styles;
const imagesTask = require("./gulp-tasks/images").images;

const cleanDist = () => {          //очистка папки dist
    return src('./dist', { read: false }).pipe(clean());
};


exports.dev = parallel(serveTask, watchTask, series(stylesTask, scriptsTask, imagesTask));
exports.build = series(cleanDist, stylesTask, scriptsTask, imagesTask);



 