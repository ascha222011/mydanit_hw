const tabsContainer = document.getElementById("top-nav").children;
let countTab;
for (var i = 0, child; (child = tabsContainer[i]); i++) {
  child.setAttribute("id", `tab${i}`);
  child.setAttribute("onclick", `openTab(${i})`);
  countTab = i;
}

const menuButton = document.getElementById(`tab${countTab}`);
menuButton.setAttribute("onclick", `showHideMenu()`);

function openTab(tabIndex) {
  for (let i = 0; i < tabsContainer.length; i++) {
    if (+tabsContainer[i].id.substring(3) === tabIndex) {
      document.getElementById(`tab${i}`).classList.add("active");
    } else {
      document.getElementById(`tab${i}`).classList.remove("active");
    }
  }
}

function showHideMenu() {
    const x = document.getElementById("top-nav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}