
/*
class Patient {

  constructor(name, birthdate, sex) {
    this.name = name;
    this.birthdate = birthdate;
    this.sex = sex;
  }

  toStriing() {
    alert(this.name);
  }

}

class NewPatient extends Patient {
  super(name, birthdate, sex);
}


// Использование:
let user = new User("Иван");
user.sayHi();

 const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('foo');
  }, 300);
});

promise1.then((value) => {
  console.log(value);
  // expected output: "foo"
});

console.log(promise1);


// expected output: [object Promise]
/* const createNewUser = () => {
 
    let firstName, lastName;
    do {
      firstName = prompt("Please, enter first name of user:", firstName)?.trim();
      lastName = prompt("Please, enter last name of user:", lastName)?.trim();
    } while (!firstName || !lastName);

    const newUser = {
      firstName,
      lastName,
      getLogin() {
        return (firstName[0]+lastName).toLowerCase();
      }
    }
  return newUser;
}

const firstUser = createNewUser();
const login = firstUser.getLogin();
console.log("Your temporary login is "+ login);  


/_ ES5 _/
var isMomHappy = false;

// Promise
var willIGetNewPhone = new Promise(
    function (resolve, reject) {
        if (isMomHappy) {
            var phone = {
                brand: 'Samsung',
                color: 'black'
            };
            resolve(phone); // Всё выполнено
        } else {
            var reason = new Error('mom is not happy');
            reject(reason); // reject
        }

    }
);

var askMom = function () {
  willIGetNewPhone
  .then(showOff) // связываем
  .then(function (fulfilled) {
          console.log(fulfilled);
       // output: 'Hey friend, I have a new black Samsung phone.'
      })
      .catch(function (error) {
          // oops, mom don't buy it
          console.log(error.message);
       // output: 'mom is not happy'
      });
};

var showOff = function (phone) {
  return new Promise(
      function (resolve, reject) {
          var message = 'Hey friend, I have a new ' +
              phone.color + ' ' + phone.brand + ' phone';

          resolve(message);
      }
  );
};

askMom();
*/


function myAsyncFunction(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = () => resolve(xhr.responseText);
    xhr.onerror = () => reject(xhr.statusText);
    xhr.send();
  });
}