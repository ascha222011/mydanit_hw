function filterBy(arr, type) {
  type.toLowerCase();
  let resArray = [];

  arr.forEach((element) => {
    // null должнен выводится при всех type кроме null
    if ((type === "object" && element === null) || (!(type === "null" && element === null) && typeof element !== type)) {
      resArray.push(element);
    }
  });
  return resArray;
};

// 23 null
console.log("Filter 'hello', 'world', 23, '23', null by string");
const res1 = filterBy(['hello', 'world', 23, '23', null], "string");
res1.forEach(element => console.log(element));

// null is object world 23 23
console.log("Filter [1,2,4], 'world', 23, '23', null by object");
const res2 = filterBy([[1,2,4], 'world', 23, '23', null], "object");
res2.forEach(element => console.log(element));

// null is object
console.log("Filter [1,2,4], 'world', 23, '23', null by number");
const res3 = filterBy([[1,2,4], 'world', 23, '23', null], "number");
res3.forEach(element => console.log(element));

// null is object
console.log("Filter [1,2,4], 'world', 23, '23', null by null");
const res4 = filterBy([[1,2,4], 'world', 23, '23', null], "null");
res4.forEach(element => console.log(element));