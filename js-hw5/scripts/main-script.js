const createNewUser = () => {
 
    let firstName, lastName, birthday;
    do {
      firstName = prompt("Please, enter first name of user:", firstName)?.trim();
      lastName = prompt("Please, enter last name of user:", lastName)?.trim();
      birthday = prompt("Please, enter user's birthday:", birthday)?.trim();
    } while (!firstName || !lastName || !birthday);

    const newUser = {
      firstName,
      lastName,
      birthday,
      getLogin() {
        return (firstName[0]+lastName).toLowerCase();
      },
      getAge() {
        birthday = new Date(birthday);
        let today = new Date();
        return today.getFullYear() - birthday.getFullYear();
      },
      getPassword() {
        return firstName[0].toUpperCase()+ lastName.toLowerCase()+ String(birthday.getFullYear());
      }
    }
  return newUser;
}

const firstUser = createNewUser();
const login = firstUser.getLogin();
const age = firstUser.getAge();
const password = firstUser.getPassword();
console.log("Your temporary login is "+ login); 
console.log("Your age is "+ age); 
console.log("Your password is "+ password); 
