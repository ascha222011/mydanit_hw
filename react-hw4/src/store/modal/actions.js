import { TOGGLE_MODAL } from "./constants";

export const toggleModalAction = (header, text, okHandler, cancelHandler) => (dispatch) => {
  dispatch({ type: TOGGLE_MODAL, header, text, okHandler, cancelHandler });

  /*
  const closeMenu = () => {
    document.removeEventListener("mouseup", closeMenu);
  };

  document.addEventListener("mouseup", closeMenu);

  document
    .getElementById("modal-actions-container")
    .addEventListener("mouseup", (e) => e.stopPropagation());*/
};
