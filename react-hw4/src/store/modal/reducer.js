import { TOGGLE_MODAL } from "./constants";

const initialStore = {
  isVisible: false,
  header: "",
  text: "",
  okHandler: null,
  cancelHandler: null,
};

const modal = (store = initialStore, action) => {
  switch (action.type) {
    case TOGGLE_MODAL:
      return {
        ...store,
        isVisible: !store.isVisible,
        header: action.header,
        text: action.text,
        okHandler: action.okHandler,
        cancelHandler: action.cancelHandler,
      };
    default:
      return store;
  }
};

export default modal;
