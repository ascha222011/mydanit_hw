import { combineReducers } from "redux";
import products from '../products/reducer';
import modal from '../modal/reducer';

const reducer = combineReducers({
  products,
  modal
})

export default reducer;
