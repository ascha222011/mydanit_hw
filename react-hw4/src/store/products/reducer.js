import { GET_PRODUCTS} from './constants';

const initialStore = {
  data: [],
};

const products = (store = initialStore, action) => {
  switch (action.type) {
    case GET_PRODUCTS:
      return { ...store, data: action.payload };
    default:
      return store;
  }
};

export default products;
