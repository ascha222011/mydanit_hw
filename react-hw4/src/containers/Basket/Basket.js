import axios from "axios";
import React, { Component, useEffect, useState } from "react";
import ProductCards from "../../components/ProductCards/ProductCards";

const Basket = () => {
  const [basketProducts, setBasketProducts] = useState([]);

  const getBasketProducts = (products) => {
    const basketProductsId = localStorage.getItem("addedProductsToCart");

    const basketProducts = products.filter((product) =>
      basketProductsId.includes(product.id)
    );
    setBasketProducts(basketProducts);
  };

  const getProducts = () => {
    axios.get("/products.json").then((res) => {
      getBasketProducts(res.data);
    });
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <div>
      <ProductCards page="basket" products={basketProducts} />
    </div>
  );
};

export default Basket;
