import axios from "axios";
import React, { Component, useEffect, useState } from "react";
import { getProductsAction } from "../../store/products/actions";
import ProductCards from "../../components/ProductCards/ProductCards";
import { connect } from "react-redux";
import { productsData } from "../../store/products/selectors";

const Home = (props) => {
  const { getProducts, products } = props;

  //const [products, setProducts] = useState([]);

  useEffect(() => {
    if (products.length === 0) getProducts();
  }, [getProducts]);

  return (
    <div>
      <ProductCards page="allProducts" products={products} />
    </div>
  );
};


const mapStoreToProps = (store) => ({
  products: productsData(store),
});

const mapDispatchToProps = (dispatch) => ({
  getProducts: () => dispatch(getProductsAction()),
});

export default connect(mapStoreToProps, mapDispatchToProps)(Home);
