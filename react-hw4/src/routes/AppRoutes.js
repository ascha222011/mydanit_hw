import React from "react";
import { Route, Switch , Redirect} from "react-router-dom";
import Basket from "../containers/Basket/Basket";
import Header from "../components/Header/Header";
import Home from "../containers/Home/Home";
import Favorites from "../containers/Favorites/Favorites";

const AppRoutes = () => {
  return (
    <div>
      <Route path="/" component={Header} />
      <Redirect to='/home' />
      <Switch>
        <Route exact path="/home" component={Home} />
        <Route exact path="/basket" component={Basket} />
        <Route exact path="/favorites" component={Favorites} />
      </Switch>
    </div>
  );
};

export default AppRoutes;
