import React from "react";
import * as icons from "../../theme/icons";
import PropTypes from "prop-types";

function Icon(props) {
  const { type, color, filled, onClick } = props;

  const iconJsx = icons[type];

  const styles = {
    margin: "20px",
  };

  if (!iconJsx) {
    return null;
  }

  return (
    <span style={styles} onClick={onClick}>
      {iconJsx(color, filled)}
    </span>
  );
}

Icon.propTypes = {
  type: PropTypes.string.isRequired,
  color: PropTypes.string,
  filled: PropTypes.bool
};

Icon.defaultProps = {
  type: "star",
  color: "grey",
  filled: true,
  onClick: "",
};

export default Icon;
