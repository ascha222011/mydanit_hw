import React, { PureComponent } from "react";
import "./Modal.scss";
import { connect } from "react-redux";
import { isModalVisibleData } from "../../store/modal/selectors";
import { toggleModalAction } from "../../store/modal/actions";

function Modal(props) {
  //const { id, name, price, image, code, color } = props.product;

  const {
    header,
    text,
    closebuton,
    handleClose,
    actions,
    isModalVisible,
  } = props;



  return (
  
    <>
      {isModalVisible && (
        <div className="modal display-block">
          <section className="modal-main">
            <div className="header-container">
              <p className="header-title">{header}</p>
              {closebuton && (
                <button className="x-button" onClick={handleClose}>
                  X
                </button>
              )}
            </div>
            <p className="main-text"> {text}</p>
            <div
              id="modal-actions-container"
              className="modal-actions-container"
            >
              {actions}
            </div>
          </section>
        </div>
      )}
    </>
  );
}

const mapStoreToProps = (store) => ({
  isModalVisible: isModalVisibleData(store),
});


export default connect(mapStoreToProps)(Modal);
