import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import "./Product.scss";
import Icon from "../Icon/Icon";
import { connect } from "react-redux";
import { isModalVisibleData } from "../../store/modal/selectors";
import { toggleModalAction } from "../../store/modal/actions";

function Product(props) {
  const { id, name, price, image, code, color } = props.product;

  const { page, toggleVisibility } = props;

  const getProductInCart = () => {
    const addedProductsToCartJSON = localStorage.getItem("addedProductsToCart");
    return JSON.parse(addedProductsToCartJSON) || [];
  };

  const checkIsAddedToCart = () => {
    const addedProductsToCart = getProductInCart();
    return addedProductsToCart.includes(id);
  };

  const getFavoriteProducts = () => {
    const markedAsFavoriteProductsJSON = localStorage.getItem(
      "markedAsFavoriteProducts"
    );
    return JSON.parse(markedAsFavoriteProductsJSON) || [];
  };

  const checkIsFavorite = () => {
    const markedProductAsFavorite = getFavoriteProducts();
    return markedProductAsFavorite.includes(id);
  };

  const [isAddedToCart, setIsAddedToCart] = useState(checkIsAddedToCart());
  const [isFavorite, setIsFavorite] = useState(checkIsFavorite());
  const [isVisible, setIsVisible] = useState(true);

  //const modalFirst = React.createRef();

  const openCartConfirmationModal = (e) => {
    let header, text, action;

    switch (page) {
      case "allProducts":
        header = "Are you sure you want to add this item to cart?";
        text = "You can remove item from cart in any time";
        action = (e) => {
          addToCart(e, id);
        };
        break;
      case "basket":
        header = "Are you sure you want to remove this item from cart?";
        text = "You can add item to cart in any time";
        action = removeFromCart;
        break;
    }

    toggleVisibility(header, text, action, closeModal);
  };


  const closeModal = (e) => {
    console.log('sd');
    toggleVisibility();
  };

  const addToCart = (e, id) => {
    console.log("fv");
    let addedProductsToCart = [];
    if (localStorage.getItem("addedProductsToCart")) {
      addedProductsToCart = getProductInCart();
    }
    addedProductsToCart.push(id);

    setIsAddedToCart(true);

    localStorage.setItem(
      "addedProductsToCart",
      JSON.stringify(addedProductsToCart)
    );
    closeModal(e);
  };

  const fillIcon = () => {
    let markedAsFavoriteProducts = [];
    if (localStorage.getItem("markedAsFavoriteProducts")) {
      markedAsFavoriteProducts = getFavoriteProducts();
    }
    if (!checkIsFavorite()) {
      markedAsFavoriteProducts.push(id);
      setIsFavorite(true);
    } else {
      const indexInFavoritsList = markedAsFavoriteProducts.indexOf(id);
      markedAsFavoriteProducts.splice(indexInFavoritsList, 1);
      setIsFavorite(false);
    }
    localStorage.setItem(
      "markedAsFavoriteProducts",
      JSON.stringify(markedAsFavoriteProducts)
    );
  };

  const removeFromCart = (e) => {
    const addedProductsToCart = getProductInCart();
    addedProductsToCart.splice(addedProductsToCart.indexOf(id), 1);
    localStorage.setItem(
      "addedProductsToCart",
      JSON.stringify(addedProductsToCart)
    );
    setIsVisible(false);
    closeModal(e);
  };

  const removeFromFavorite = () => {
    const favoriteProducts = getFavoriteProducts();
    favoriteProducts.splice(favoriteProducts.indexOf(id), 1);
    localStorage.setItem(
      "markedAsFavoriteProducts",
      JSON.stringify(favoriteProducts)
    );
    setIsVisible(false);
  };

  if (isVisible) {
    return (
      <div className="product-container">
        {page == "basket" && (
          <div className="cross-container">
            <Icon type="cancel" onClick={openCartConfirmationModal} />
          </div>
        )}
        {page == "favorites" && (
          <div className="cross-container">
            <Icon
              filled={isFavorite ? true : false}
              type="star"
              color="#FFD700"
              onClick={removeFromFavorite}
            />
          </div>
        )}
        <img className="product-image" alt={name} src={image} />
        <h3> {name}</h3>
        <p>
          {code} / {color}
        </p>
        <h2> {price} $</h2>

        <div className="actions-container">
          {page === "allProducts" && (
            <>
              <Button
                color="white"
                backgroundColor="#0ec4ba"
                text="Add to cart"
                disabled={isAddedToCart}
                onClick={openCartConfirmationModal}
              />
              <Icon
                filled={isFavorite ? true : false}
                type="star"
                color="#FFD700"
                onClick={fillIcon}
              />
            </>
          )}
        </div>
      </div>
    );
  } else return null;
}

Product.defaultProps = {
  product: [],
  page: null,
};

Product.propTypes = {
  product: PropTypes.object.isRequired,
  page: PropTypes.string,
};

const mapStoreToProps = (store) => ({
  isModalVisible: isModalVisibleData(store),
});

const mapDispatchToProps = (dispatch) => ({
  toggleVisibility: (header, text, okHandler,closeModal) =>
    dispatch(toggleModalAction(header, text, okHandler,closeModal)),
});

export default connect(mapStoreToProps, mapDispatchToProps)(Product);
