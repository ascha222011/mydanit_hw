import React from "react";
import Product from "../Product/Product";
import PropTypes from "prop-types";
import "./ProductCards.scss";
import { connect } from "react-redux";
import { isModalVisibleData } from "../../store/modal/selectors";
import { toggleModalAction } from "../../store/modal/actions";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";

const ProductCards = (props) => {

  const { products, page, modalHeader, modalText, modalOkHandler, modalCancelHandler } = props;
  

  const productCards = products
    .filter((product) => !product.hidden)
    .map((product) => { 

    return <Product page={page} key={product.id} product={product} /> });
    

  return <> <div className="products-container">{productCards}</div> {getModal(modalHeader,modalText,modalOkHandler, modalCancelHandler )} </>;
};


const getModal = (header, text, okHandler, cancelHandler) => {

  return (
    <Modal
      header={header}
      text={text}
      handleClose={cancelHandler}
      actions={
        <>
          <Button
            color="white"
            backgroundColor="#007dff"
            text="YES!"
            onClick={okHandler}
          />
          <Button
            color="white"
            backgroundColor="#c9e5ee"
            text="Cancel"
            onClick={cancelHandler}
          />
        </>
      }
    >
      <p>Modal</p>
    </Modal>
  );
};


ProductCards.propTypes = {
  products: PropTypes.array,
};

ProductCards.defaultProps = {
  products: [],
  page: null
};

const mapStoreToProps = (store) => ({
  isModalVisible: isModalVisibleData(store),
  modalHeader: store.modal.header, 
  modalText: store.modal.text, 
  modalOkHandler: store.modal.okHandler, 
  modalCancelHandler: store.modal.cancelHandler
});

export default connect(mapStoreToProps)(ProductCards);
