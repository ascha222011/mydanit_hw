import React, { PureComponent } from 'react';
import { NavLink } from 'react-router-dom';
import "./Header.scss";

const Header = () => {
  return (
    <div className="navbar">
      <ul>
        <li><NavLink exact to='/home' className='router-link' activeClassName='router-link--active'>Home</NavLink></li>
        <li><NavLink exact to='/favorites' className='router-link' activeClassName='router-link--active'>Favorites</NavLink></li>
        <li><NavLink exact to='/basket' className='router-link' activeClassName='router-link--active'>Basket</NavLink></li>
      </ul>
    </div>
  );
}

export default Header;
