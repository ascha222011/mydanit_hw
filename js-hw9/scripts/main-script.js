const tabsContainer = document.getElementById("tabs").children;
for (var i = 0, child; (child = tabsContainer[i]); i++) {
  child.setAttribute("id", `tab${i}`);
  child.setAttribute("onclick", `openTab(${i})`);
}

const contentContainer = document.getElementById("tabs-content").children;
for (var i = 0, child; (child = contentContainer[i]); i++) {
  child.setAttribute("id", `${i}`);
}

function openTab(tabIndex) {
  console.log(tabIndex);

  for (let i = 0; i < tabsContainer.length; i++) {
    if (+tabsContainer[i].id.substring(3) === tabIndex) {
      document.getElementById(`tab${i}`).classList.add("active");
    } else {
      document.getElementById(`tab${i}`).classList.remove("active");
    }
  }

  for (let i = 0; i < contentContainer.length; i++) {
    if (contentContainer[i].id != tabIndex) {
      document.getElementById(i).hidden = true;
    } else document.getElementById(i).hidden = false;
  }
}
