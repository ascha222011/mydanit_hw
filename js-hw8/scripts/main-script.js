if (!price.value) {
  document.getElementById("resetButton").hidden = true;
}

price.onblur = function () {
  if (validation()) {
    if (price.value) {
      currentValueOfPrice.innerHTML = `Текущая цена: ${price.value}`;
      document.getElementById("resetButton").hidden = false;
    } else {
      currentValueOfPrice.innerHTML = "Введите цену.";
    }
    document.getElementById("price").style.border = "1px solid black";
    areaForError.innerHTML = "";
  }
};

function resetInput() {
  price.value = "";
  currentValueOfPrice.innerHTML = "Введите цену.";
  document.getElementById("resetButton").hidden = true;
}

function validation() {
  if (price.value < 0) {
    document.getElementById("price").style.border = "1px solid red";
    areaForError.innerHTML = "Please enter correct price";
    currentValueOfPrice.innerHTML = "";
    return false;
  } else return true;
}
