// initial data
const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];
const requiredFields = ["author", "name", "price"];

showBooks(books, requiredFields);

function showBooks(books, requiredFields) {
  const rootElement = document.getElementById("root");
  const ul = document.createElement("ul");
  rootElement.appendChild(ul);

  for (book of books) {
    try {
      let bookHtml = "";
      for (reqField of requiredFields) {
        if (!book.hasOwnProperty(reqField))
          throw new Error(`Objct dosn't have ${reqField} value`);
        bookHtml += book[reqField] + " ";
      }
      const li = document.createElement("li");

      li.innerHTML = bookHtml;
      ul.append(li);
    } catch (e) {
      console.error(e);
    }
  }
}
