import React, { Component } from "react";
import "./App.scss";
import AppRoutes from "./routes/AppRoutes.js";

const App = () => {
  return (
    <div>
      <AppRoutes />
    </div>
  );
};

export default App;
