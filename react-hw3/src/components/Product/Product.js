import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import "./Product.scss";
import Icon from "../Icon/Icon";

function Product(props) {
  const { id, name, price, image, code, color } = props.product;
  const { page } = props;

  const getProductInCart = () => {
    const addedProductsToCartJSON = localStorage.getItem("addedProductsToCart");
    return JSON.parse(addedProductsToCartJSON) || [];
  };

  const checkIsAddedToCart = () => {
    const addedProductsToCart = getProductInCart();
    return addedProductsToCart.includes(id);
  };

  const getFavoriteProducts = () => {
    const markedAsFavoriteProductsJSON = localStorage.getItem(
      "markedAsFavoriteProducts"
    );
    return JSON.parse(markedAsFavoriteProductsJSON) || [];
  };

  const checkIsFavorite = () => {
    const markedProductAsFavorite = getFavoriteProducts();
    return markedProductAsFavorite.includes(id);
  };

  const [isAddedToCart, setIsAddedToCart] = useState(checkIsAddedToCart());
  const [isFavorite, setIsFavorite] = useState(checkIsFavorite());
  const [isVisible, setIsVisible] = useState(true);

  const modalFirst = React.createRef();

  const openCartConfirmationModal = (e) => {
    modalFirst.current.handleClick(e);
  };

  const closeMenu = (e) => {
    modalFirst.current.closeMenu(e);
  };

  const addToCart = (e) => {
    let addedProductsToCart = [];
    if (localStorage.getItem("addedProductsToCart")) {
      addedProductsToCart = getProductInCart();
    }
    addedProductsToCart.push(id);
    setIsAddedToCart(true);

    localStorage.setItem(
      "addedProductsToCart",
      JSON.stringify(addedProductsToCart)
    );
    closeMenu(e);
  };

  const fillIcon = () => {
    let markedAsFavoriteProducts = [];
    if (localStorage.getItem("markedAsFavoriteProducts")) {
      markedAsFavoriteProducts = getFavoriteProducts();
    }
    if (!checkIsFavorite()) {
      markedAsFavoriteProducts.push(id);
      setIsFavorite(true);
    } else {
      const indexInFavoritsList = markedAsFavoriteProducts.indexOf(id);
      markedAsFavoriteProducts.splice(indexInFavoritsList, 1);
      setIsFavorite(false);
    }
    localStorage.setItem(
      "markedAsFavoriteProducts",
      JSON.stringify(markedAsFavoriteProducts)
    );
  };

  const removeFromCart = () => {
    const addedProductsToCart = getProductInCart();
    addedProductsToCart.splice(addedProductsToCart.indexOf(id), 1);
    localStorage.setItem(
      "addedProductsToCart",
      JSON.stringify(addedProductsToCart)
    );
    setIsVisible(false);
  };

  const removeFromFavorite = () => {
    const favoriteProducts = getFavoriteProducts();
    favoriteProducts.splice(favoriteProducts.indexOf(id), 1);
    localStorage.setItem(
      "markedAsFavoriteProducts",
      JSON.stringify(favoriteProducts)
    );
    setIsVisible(false);
  };

  const getModal = (type) => {
    let header, text, action;

    switch (type) {
      case "allProducts":
        header = "Are you sure you want to add this item to cart?";
        text = "You can remove item from cart in any time";
        action = addToCart;
        break;
      case "basket":
        header = "Are you sure you want to remove this item from cart?";
        text = "You can add item to cart in any time";
        action = removeFromCart;
        break;
    }

    return (
      <Modal
        ref={modalFirst}
        header={header}
        text={text}
        handleClose={closeMenu}
        actions={
          <>
            <Button
              color="white"
              backgroundColor="#007dff"
              text="YES!"
              onClick={action}
            />
            <Button
              color="white"
              backgroundColor="#c9e5ee"
              text="Cancel"
              onClick={closeMenu}
            />
          </>
        }
      >
        <p>Modal</p>
      </Modal>
    );
  };

  if (isVisible) {
    return (
      <div className="product-container">
        {page == "basket" && (
          <div className="cross-container">
            <Icon type="cancel" onClick={openCartConfirmationModal} />
          </div>
        )}
        {page == "favorites" && (
          <div className="cross-container">
          <Icon
                filled={isFavorite ? true : false}
                type="star"
                color="#FFD700"
                onClick={removeFromFavorite}
              />
          </div>
        )}
        <img className="product-image" alt={name} src={image} />
        <h3> {name}</h3>
        <p>
          {code} / {color}
        </p>
        <h2> {price} $</h2>

        <div className="actions-container">
          {page === "allProducts" && (
            <>
              <Button
                color="white"
                backgroundColor="#0ec4ba"
                text="Add to cart"
                disabled={isAddedToCart}
                onClick={openCartConfirmationModal}
              />
              <Icon
                filled={isFavorite ? true : false}
                type="star"
                color="#FFD700"
                onClick={fillIcon}
              />
            </>
          )}
        </div>

        {getModal(page)}
      </div>
    );
  } else return null;
}

Product.defaultProps = {
  product: [],
  page: null,
};

Product.propTypes = {
  product: PropTypes.object.isRequired,
  page: PropTypes.string,
};

export default Product;
