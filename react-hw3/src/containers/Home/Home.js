import axios from "axios";
import React, { Component, useEffect, useState } from "react";
import Header from "../../components/Header/Header";
import ProductCards from "../../components/ProductCards/ProductCards";

const Home = () => {
  const [products, setProducts] = useState([]);

  const getProducts = () => {
    axios.get("/products.json").then((res) => {
      setProducts(res.data);
    });
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <div>
      <ProductCards page="allProducts" products={products} />
    </div>
   
  );
};

export default Home;
