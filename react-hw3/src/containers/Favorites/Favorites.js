import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import ProductCards from "../../components/ProductCards/ProductCards";

const Favorites = () => {

  const [favoriteProducts, setFavoriteProducts] = useState([]);

  const getFavoriteProducts = (products) => {
    const favoriteProductsId = localStorage.getItem("markedAsFavoriteProducts");
    const favoriteProducts = products.filter((product) =>
      favoriteProductsId.includes(product.id)
    );
    setFavoriteProducts(favoriteProducts);
  };

  const getProducts = () => {
    axios.get("/products.json").then((res) => {
      getFavoriteProducts(res.data);
    });
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <div>
      <ProductCards page="favorites" products={favoriteProducts} />
    </div>
  );
};

export default Favorites;
