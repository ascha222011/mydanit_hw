// initial data
const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human",
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire",
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire",
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire",
  },
];
const user1 = {
  name: "John",
  years: 30,
};
const satoshi2020 = {
  name: "Nick",
  surname: "Sabo",
  age: 51,
  country: "Japan",
  birth: "1979-08-21",
  location: {
    lat: 38.869422,
    lng: 139.876632,
  },
};

const satoshi2019 = {
  name: "Dorian",
  surname: "Nakamoto",
  age: 44,
  hidden: true,
  country: "USA",
  wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
  browser: "Chrome",
};

const satoshi2018 = {
  name: "Satoshi",
  surname: "Nakamoto",
  technology: "Bitcoin",
  country: "Japan",
  browser: "Tor",
  birth: "1975-04-05",
};

const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Game of thrones",
  author: "George R. R. Martin",
};

const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

// Task 1
function taskFirst(clients1, clients2) {
  const clients3 = clients1.concat(clients2);
  const resultUniqueClients = [...new Set(clients3)];
  return resultUniqueClients;
}

const resultUniqueClients = taskFirst(clients1, clients2);
// result display
blockForClients = document.getElementById("clients");
blockForClients.innerHTML = resultUniqueClients.toString();

// Task 2
function taskSecond(characters) {
  const resultsCharacters = characters.map(function (num) {
    let { name, lastName, age } = num;
    return { name, lastName, age };
  });
  return resultsCharacters;
}
resultUniqueCharacters = taskSecond(characters);
// result display
blockForClients = document.getElementById("characters");
let resultList = "";
resultUniqueCharacters.map((num) => {
  for (const [key, value] of Object.entries(num)) {
    resultList += `${key}: ${value} <br>`;
  }
});
blockForClients.innerHTML = resultList;

// Task 3
function taskThird(user1) {
  let { name, years, isAdmin = false } = user1;
  return { name, years, isAdmin };
}
let resultThirdTask = taskThird(user1);
// result display
let destructedUserblock = document.getElementById("user");
let resultThirdTaskforDisplay = "";
for (const [key, value] of Object.entries(resultThirdTask)) {
    resultThirdTaskforDisplay += `${key}: ${value} <br>`;
}
destructedUserblock.innerHTML = resultThirdTaskforDisplay;


// Task 4
function taskFourth() {
  return (satoshiCombined = { ...satoshi2018, ...satoshi2019, ...satoshi2020 });
}
const resultFourthTask = taskFourth();
// result display
let biographyBlock = document.getElementById("biography");
biographyBlock.innerHTML = JSON.stringify(resultFourthTask);

// Task 5
function taskFifth() {
  const booksCombined = [...books, bookToAdd];
  return booksCombined;
}
const resultFifthTask = taskFifth();
// result display
let booksBlock = document.getElementById("books");
booksBlock.innerHTML = JSON.stringify(resultFifthTask);

// Task 6
function taskSixth() {
  const newEmployee = { ...employee, age: 10, salary: 20 };
  return newEmployee;
}
const resultSixthTask = taskSixth();
let employeeBlock = document.getElementById("employee");
let resultSixthTaskforDisplay = "";
for (const [key, value] of Object.entries(resultSixthTask)) {
  resultSixthTaskforDisplay += `${key}: ${value} <br>`;
}
employeeBlock.innerHTML = resultSixthTaskforDisplay;

// Task 7
function taskSeventh () {
    const array = ['value', () => 'showValue'];
    let [value, showValue] = array;
    alert(value);
    alert(showValue());  
}
taskSeventh();
