let firstOperand, secondOperand, operator;
do {
  if (firstOperand == null) firstOperand = "";
  if (secondOperand == null) secondOperand = "";
  firstOperand = prompt(
    "Please, enter valid value of firstOperand:",
    firstOperand
  );
  secondOperand = prompt(
    "Please, enter valid value of secondOperand:",
    secondOperand
  );
} while (!isValidNumber(firstOperand) || !isValidNumber(secondOperand));

do {
  if (operator == null) operator = "";
  operator = prompt("Please, enter valid value of operator:", operator);
} while (
  !isValidOperator(operator)
);

console.log(calculateValue(+firstOperand, +secondOperand, operator));

function calculateValue(firstOperand, secondOperand, operator) {
  let res;
  switch (operator) {
    case "+":
      res = firstOperand + secondOperand;
      break;
    case "-":
      res = firstOperand - secondOperand;
      break;
    case "*":
      res = firstOperand * secondOperand;
      break;
    case "/":
      if (secondOperand == 0) {
        console.log("Division on 0 :(");
        return null;
      }
      res = firstOperand / secondOperand;
      break;
  }
  return res;
}

function isValidNumber(number) {
  return number && Number.isInteger(+number);
}

function isValidOperator(operator){
 const possibleOperators = ["/", "*", "-", "+"];
 return possibleOperators.includes(operator);
}