let userName = prompt("Your name:", "");

// валидация по имени - не должно быть пустым, поэтому в default name не userName, a ""
while (!userName) {
  userName = prompt(
    "Your name should  not be empty:( Could you try again, please:", "");
}

let userAge = prompt("Your age:", "");

// валидация по возрасту - должно быть числом + проверка на null когда юзер нажимает cancel
while (isNaN(userAge) || userAge == null) {
  if (userAge == null) userAge = "";
  userAge = prompt("Your age is incorrect:( Could you recheck it, please:", userAge);
}

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
  const isAgreed = confirm("Are you sure you want to continue?");
  if (isAgreed) {
    alert(`Welcome, ${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Welcome, ${userName}`);
}
