const resArray = [];
let number;

// валидация - должно быть целым числом + проверка на null когда юзер нажимает cancel
do {
  if (number == null) number = "";
  number = +prompt("Please, enter valid value of number:", number);
} while (!Number.isInteger(+number) || number == null);

// начинаем цикл с 1 потому что наш диапозон (0; number) обе границы не включительные
for (let i = 1; i < number; i++) {
  if (i % 5 == 0) {
    resArray.push(i);
  }
}

if (resArray.length == 0) {
  console.log("Sorry, no numbers");
} else resArray.forEach((element) => console.log(element));

/*
const resArrayTask2 = [];
let m, n;

do {
  if (m == null) m = "";
  m = +prompt("Enter a valid lower number:", m);
} while (!Number.isInteger(m) || m == null);

do {
  if (n == null) n = "";
  n = +prompt("Please, enter valid larger number:", n);
} while (!Number.isInteger(n) || n == null);

for (let i = m; i < n; i++) {
  if (isSimple(i) == true) {
    resArrayTask2.push(i);
  }
}

if (resArrayTask2.length == 0) {
  console.log("Sorry, no numbers");
} else resArrayTask2.forEach((element) => console.log(element));

function isSimple(number) {
  for (let i = 2; i <= Math.sqrt(number); i++) {
    if (number % i == 0) return false;
  }
  return true;
}
*/
